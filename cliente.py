import socket
import sys

from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, QLineEdit, QLabel

host = '157.230.14.37'
port = 8400


# Interfaz del cliente
class InterfazCliente(QWidget):

    def __init__(self):
        self.username = ''
        self.password = ''
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 600, 220)

        # Label del usuario
        self.userL = QLabel(self)
        self.userL.move(20, 20)
        self.userL.setText('Usuario:')

        # Campo para escribir el usuario
        self.username = QLineEdit(self)
        self.username.move(20, 40)
        self.username.resize(200, 20)
        self.username.textChanged.connect(self.setUsername)

        # Label de la contraseña
        self.passwordL = QLabel(self)
        self.passwordL.move(20, 80)
        self.passwordL.setText('Contraseña:')

        # Campo para escribir la contraseña
        self.password = QLineEdit(self)
        self.password.move(20, 100)
        self.password.resize(200, 20)
        self.password.textChanged.connect(self.setPassword)

        # Boton para autenticarse, llama al metodo conectar
        self.button = QPushButton('Conectar', self)
        self.button.move(20, 140)
        self.button.clicked.connect(self.conectar)

        # Label del estado de la conexión
        self.estadoL = QLabel(self)
        self.estadoL.move(320, 20)
        self.estadoL.setText('Estado de la conexión: ')

        # Estado de la conexión
        self.estado = QLabel(self)
        self.estado.move(320, 40)
        self.estado.setText('No se ha realizado la autenticación')

        self.show()

    # Metodo que realiza la conexión según el usuario y contraseña escritos, Actualiza el estado mostrado según el mensaje
    # que recibe del servidor
    def conectar(self):
        s = socket.socket()
        s.connect((host, port))

        s.send(bytes('%s,%s' % (self.username, self.password), 'utf-8'))

        mensajeServidor = s.recv(1024).decode('utf-8')
        self.estado.setText(mensajeServidor)

        s.close()
        return mensajeServidor

    def setUsername(self, username):
        self.username = username

    def setPassword(self, password):
        self.password = password


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = InterfazCliente()
    sys.exit(app.exec_())
