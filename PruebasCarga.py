import socket
import threading
import time
import matplotlib.pyplot as plt

# PROGRAMA QUE REALIZA LAS PRUEBAS DE CARGA SOBRE EL SERVIDOR

s = socket.socket()
# host = 'localhost'
host = '157.230.14.37'
port = 8400

tiempos = []
errores = 0
lockTiempo = threading.Lock()
lockError = threading.Lock()

# Se envia la autenticación al servidor
def enviarAutenticacion(s: socket.socket, user, passw):
    s.send(bytes('%s,%s' % (user, passw), 'utf-8'))


# Funciones usadas para medir el desempeño del servidor durante las pruebas
def anotarTiempo(tiempo):
    global tiempos
    global lockTiempo

    lockTiempo.acquire()
    tiempos.append(tiempo)
    lockTiempo.release()


def anotarError():
    global errores
    global lockError

    lockError.acquire()
    errores += 1
    lockError.release()


# Inicia una conexión con el servidor
def conectarCliente(numCliente):
    tI = time.time()
    try:
        # Se conecta mediante el socket
        s = socket.socket()
        s.connect((host, port))

        # Calcula el numero de cliente % 2000 ya que solo hay 2000 en la base de datos
        numCliente = numCliente % 2000
        # Envia una autenticación valida según su numero de cliente
        enviarAutenticacion(s, 'usuario%d' % numCliente, "pass%d" % numCliente)
        # recibe el mensaje del servidor
        mensajeServidor = s.recv(1024).decode('utf-8')
        # cierra la conexión
        s.close()

    # Actualiza la cantidad de errores y el tiempo que tardo en autenticarse
    except:
        anotarError()
    anotarTiempo(time.time() - tI)


# Inicia una prueba con un numero de threads y un ramp up dado
def iniciarPrueba(numThreads, rampUp):
    ts = []

    # Inicia los threads y espera a que el ultimo termine
    for i in range(numThreads):
        ts.append(threading.Thread(target=conectarCliente, args=(i,)))

    for i in range(numThreads):
        ts[i].start()
        # Administra la frecuencia de inicio de clientes según el tiempo de ramp up
        time.sleep(rampUp / numThreads)

    ts[numThreads - 1].join()


if __name__ == '__main__':
    # Escenarios propuestos según su numero de peticiones
    escenarios = [10, 10, 1000, 1000, 2000, 2000, 3000, 3000, 5000, 5000, 7000, 7000, 10000, 10000]
    # Tiempo de ramp up
    rampUp = 10

    # Variables para guardar la latencia promedio y el porcentaje de errores de cada prueba
    latenciasPromedio = []
    pErrores = []
    for numThreads in escenarios:
        # Inicia una prueba
        iniciarPrueba(numThreads, rampUp)

        # Calcula la latencia promedio de los clientes iniciados durante la prueba y el porcentaje de error
        latPromedio = 1000*sum(tiempos) / len(tiempos)
        pError = 100 * errores / numThreads

        latenciasPromedio.append(latPromedio)
        pErrores.append(pError)

        tiempos = []
        errores = 0

        print('%d threads - %.2f latencia promedio - %.2f pError' % (numThreads, latPromedio, pError))

    # Promedia los resultados de las dos iteraciones de la misma prueba
    escenarios2 = []
    latenciasPromedio2 = []
    pErrores2 = []
    for i in range(0, len(escenarios), 2):
        escenarios2.append(escenarios[i])
        latenciasPromedio2.append((latenciasPromedio[i] + latenciasPromedio[i + 1]) / 2)
        pErrores2.append((pErrores[i] + pErrores[i + 1]) / 2)

    # Genera una gráfica con los resultados de las pruebas
    fig, ax1 = plt.subplots()

    color = 'tab:red'
    ax1.set_xlabel('Número de Transacciones')
    ax1.set_ylabel('Latencia promedio (ms)', color=color)
    ax1.plot(escenarios2, latenciasPromedio2, color=color)
    ax1.tick_params(axis='y', labelcolor=color)

    ax2 = ax1.twinx()

    color = 'tab:blue'
    ax2.set_ylabel('% Error', color=color)
    ax2.plot(escenarios2, pErrores2, color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    ax1.set_title('Resultados Pruebas de Escalabilidad')

    fig.tight_layout()
    plt.show()
