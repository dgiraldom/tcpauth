import socket
import threading
import time
import random
import psycopg2
import hashlib

# Cuando esto es True se limitan las conexiones por minuto que provienen desde la misma IP,
#  se deshabilita para las pruebas de carga
prevenirDoS = False

tiempoTotalCola = 0
clientesAtendidos = 0
clientesEspera = 0

# Crea una conexión con la base de datos
def conectarDB() -> psycopg2.extensions.connection:
    conn = psycopg2.connect(
        database='infracom',
        user='infracom',
        password='infracom',
        host='157.230.14.37',
        port='5432',
    )
    return conn

# Devuelve el hash de la contraseña que entra como parametro usando el algoritmo sha1
def getPasswordHash(password):
    m = hashlib.sha1()
    m.update(bytes(password, 'utf-8'))
    passwordHash = m.hexdigest()
    return passwordHash

# Realiza el proceso de autenticación dado un usuario y una contraseña
# Devuelve True si la autenticación es exitosa y false de lo contrario
def autenticar(user, passw, conn: psycopg2.extensions.connection):
    cur = conn.cursor()
    cur.execute("SELECT * FROM auth WHERE username='%s';" % user)
    userInfo = cur.fetchone()

    if userInfo is None:
        return False

    passwordDB = userInfo[1]
    passwordHash = getPasswordHash(passw)

    cur.close()
    return passwordDB == passwordHash

# ______________________________________________________________________________________________________________________
# FUNCIONES USADAS PARA ACTUALIZAR LAS MEDIDAS DEL ESTADO DE LAS PETICIONES
def aumentarTiempoTotal(incrementoTiempo):
    global tiempoTotalCola
    lockTiempoTotal.acquire()
    tiempoTotalCola += incrementoTiempo
    lockTiempoTotal.release()


def aumentarClientesEspera():
    global clientesEspera
    lockClientesEspera.acquire()
    clientesEspera += 1
    lockClientesEspera.release()


def disminuirClientesEspera():
    global clientesEspera
    lockClientesEspera.acquire()
    clientesEspera -= 1
    lockClientesEspera.release()


def aumentarClientesAtendidos():
    global clientesAtendidos
    lockClientesAtendidos.acquire()
    clientesAtendidos += 1
    lockClientesAtendidos.release()

# Inicia la conexion con el cliente
def conexion(c: socket, address, conn):
    # Tiempo en el que inicia la conexión
    tI = time.time()
    aumentarClientesEspera()

    # Recibe un mensaje con el usuario y contraseña
    authBytes = c.recv(1024)
    authStr = authBytes.decode('utf-8')
    authArray = authStr.split(',')
    # Autentica el usuario
    authResult = autenticar(authArray[0], authArray[1], conn)

    # Envia el estado de la autenticación dependiendo del resultado anterior
    if authResult:
        c.send(bytes('Autenticación exitosa', 'utf-8'))
        print('Autenticación exitosa del usuario: ', authArray[0])
    else:
        c.send(bytes('Autenticación rechazada', 'utf-8'))
        print('Autenticación rechazada del usuario: ', authArray[0])

    # Cierra la conexión con el usuario
    c.close()

    # Guarda y muestra el estado de los clientes
    tiempoTranscurrido = time.time() - tI
    aumentarClientesAtendidos()
    disminuirClientesEspera()
    aumentarTiempoTotal(tiempoTranscurrido)
    print('Clientes en espera: %d - Clientes Atendidos: %d - Tiempo Promedio Espera: %.2f segundos' % (
        clientesEspera, clientesAtendidos, tiempoTotalCola / clientesAtendidos))


# Función que se encarga de resetear el conteo de conexiones cada minuto
def administrarConexiones():
    while True:
        time.sleep(60)
        conexionesPorMinuto = {}


if __name__ == '__main__':
    # Se crea una conexion con la base de datos
    conn = conectarDB()
    # Se crea un socket que acepta conexiones de todas las IPs
    s = socket.socket()
    host = '0.0.0.0'
    port = 8400
    s.bind((host, port))

    # Se inician los locks usados para escribir sobre las variables compartidas de medición
    lockTiempoTotal = threading.Lock()
    lockClientesEspera = threading.Lock()
    lockClientesAtendidos = threading.Lock()

    # Se crea un diccionario usado para medir las conexiones recibidas en el ultimo minuto y se inicia el metodo que lo
    # administra
    conexionesPorMinuto = {}
    threading.Thread(target=administrarConexiones).start()

    s.listen()
    while True:
        # Acepta las conexiones de los clientes entrantes
        c, addr = s.accept()

        # Bloque usado para prevenir un ataque de denial of service
        if prevenirDoS:
            # Se obtiene la ip del cliente
            ip = addr[0]
            # Se aumenta el contados de intentos de conexión provenientes de esa IP
            if ip in conexionesPorMinuto:
                conexionesPorMinuto[ip] += 1
            else:
                conexionesPorMinuto[ip] = 0
            # Si se trato de conectar más de 6 veces en el ultimo minuto no se acepta la conexión
            if conexionesPorMinuto[ip] > 6:
                c.close()
            # De lo contrario se inicia un thread en el que se realiza el proceso de autenticación
            else:
                threading.Thread(target=conexion, args=(c, addr, conn)).start()

        else:
            threading.Thread(target=conexion, args=(c, addr, conn)).start()
