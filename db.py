from servidor import conectarDB
import hashlib

conn = conectarDB()

cur = conn.cursor()
for i in range(2000):
    user = 'usuario' + str(i)
    password = 'pass' + str(i)
    m = hashlib.sha1()
    m.update(bytes(password, 'utf-8'))
    passwordHash = m.hexdigest()
    cur.execute("INSERT INTO auth (username, password) VALUES ('%s','%s')" % (user, passwordHash))

conn.commit()
